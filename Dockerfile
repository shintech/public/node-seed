FROM node:16.14.0

# RUN useradd -m -d /home/web web && \
#   mkdir -p /home/web/app/node_modules /home/web/app/log && \
#   chown -R web:web /home/web

WORKDIR /home/node

RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 && \
  chmod +x /usr/local/bin/dumb-init

COPY package.json .
COPY yarn.lock .

USER node

RUN yarn install --production=true

COPY --chown=node:node . .

ENTRYPOINT ["dumb-init", "--"]

CMD yarn start
