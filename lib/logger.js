import path from 'path';
import date from 'date-fns';
import { createLogger, format, transports } from 'winston';
import 'winston-daily-rotate-file';

const { label, combine, colorize, printf, timestamp } = format;

const NODE_ENV = process.env['NODE_ENV'];
const PKG_NAME = process.env['npm_package_name'];
const PKG_VERSION = process.env['npm_package_version'];
const LOG_PATH = process.env['LOG_PATH'] || path.resolve('log');

const myFormat = printf(({ level, label, message, timestamp }) => {
  return `${level}: ${label} ${timestamp} -> ${message}`;
});

const datetime = date.format(new Date(), 'yyyy-MM-dd HH:MM:ss');

const DailyRotateApplication = new transports.DailyRotateFile({
  dirname: LOG_PATH,
  filename: 'application-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '14d',
  level: 'info'
});

const DailyRotateError = new transports.DailyRotateFile({
  dirname: LOG_PATH,
  filename: 'error-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '14d',
  level: 'error'
});

const logger = createLogger({
  transports: [
    DailyRotateApplication,
    DailyRotateError
  ],
  format: combine(
    label({ label: `${PKG_NAME}:${PKG_VERSION}` }),
    timestamp({
      format: datetime
    }),
    myFormat
  )
});

if (NODE_ENV !== 'test') {
  logger.add(
    new transports.Console({
      level: 'info',
      format: combine(
        colorize(),
        myFormat
      )
    }));
}

export default logger;
