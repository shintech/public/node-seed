# shintech/development/tulsatechservices

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install) <br />
	a. [.env ](#env) <br />
	b. [Development ](#development) <br />
	c. [Production ](#production) <br />
	c. [git hooks ](#git-hooks)
3. [ Usage ](#usage) <br />

<a name="synopsis"></a>
### Synopsis

Node seed project...
  
<a name="install"></a>
### Installation

    yarn install

<a name="env"></a>
#### Copy the config for the environment from extra/env/[environment].env and edit as necessary.

    NODE_ENV=production
    PORT=3000
    HOST=0.0.0.0
    
    MONGO_HOST=database
    MONGO_PORT=27017
    MONGO_INITDB_DATABASE=production
    MONGO_INITDB_ROOT_USERNAME=admin
    MONGO_INITDB_ROOT_PASSWORD=password
    
    SMTP_HOST=smtp.gmail.com
    SMTP_PORT=587
    SMTP_SENDER=test@example.org
    SMTP_PASS=abcd1234
    SMTP_BCC=test@example.org
    
    RECAPTCHA_SECRET=abcd_1234

<a name="development"></a>
#### Development

    npm run dev

    # or

    yarn dev

<a name="production"></a>
#### Production
    docker-compose build && docker-compose up -d
    
<a name="git-hooks"></a>
#### git hooks
    ln -s /path/to/repo/examples/hooks/hook /path/to/repo/.git/hooks/

<a name="usage"></a>
### Usage
