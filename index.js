import http from 'http';
import hello from './lib/hello.js';
import logger from './lib/logger.js';

const PORT = parseInt(process.env['PORT']) || 3000;
const HOST = process.env['HOST'] || '127.0.0.1';
const NODE_ENV = process.env['NODE_ENV'] || 'development';

const server = http.createServer((req, res) => {
  logger.info(`${req.method} - ${req.url}`);
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.write(JSON.stringify({ response: hello() }));
  res.end();
});

server.listen(PORT, HOST);

server.on('listening', () => {
  if (NODE_ENV === 'development') {
    logger.info(`listening at ${HOST}:${PORT}`);
  }
});
