import chai from 'chai';
import helloWorld from '../lib/hello.js';

const expect = chai.expect;

describe('Hello World Test...', () => {
  it('should say hello world!', async () => {
    expect(helloWorld()).to.equal('hello world!');
  });
});
